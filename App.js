import React from 'react';
import {StatusBar } from 'expo-status-bar';
import AppLoading from 'expo-app-loading';

import AppStack from './src/routes/AppStack';


export default function App() {
    return (
      <>
        <AppStack/>
        <StatusBar style="light"/>
      </>
    );
}
