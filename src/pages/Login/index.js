import {View, Text, Image, TextInput } from 'react-native';
import { RectButton } from 'react-native-gesture-handler';
import { useNavigation } from '@react-navigation/native';

import React from 'react';
import styles from './styles';


function Login() {
  const {navigate} = useNavigation();
  

  function  Login() {
    navigate('Plans');
  }


  return (
    <View style={styles.container}>
      <Text style={styles.welcome}>Seja bem vindo!</Text>

      <View style={styles.inputGroup}>
        <TextInput
          editable
          maxLength={40}
          style={styles.input}
          placeholder='E-mail'
          autoCompleteType='email'
        />
        <TextInput
          editable
          maxLength={40}
          style={styles.input}
          placeholder='Senha'
          autoCompleteType='password'
          secureTextEntry={true}
        />
      </View>


      <RectButton  style={styles.button_access} onPress={Login}>
        <Text style={styles.button_access_text}>Entrar</Text>
      </RectButton>
    </View>
  );
}


export default Login;