import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#FFFFFF',
    },

    background: {
      position: 'absolute',
      top: '0%',
      flex: 1,
    },

    welcome :{
        paddingTop:'35%',
        paddingLeft:'35%',
        color: '#F7A04A',
        fontFamily: 'OpenSans_700Bold',
        fontWeight: 'bold',
    },

    inputGroup: {
      flexDirection: 'column',
      justifyContent: 'space-between'
    },

    input :{
        top:'40%',
        left: '10%',
        
        backgroundColor: '#FFFFFF',
        width: '80%',
        height: 53,
        borderRadius:14,
        marginBottom: 12,

        fontFamily: 'Lato_100Thin',
        fontStyle: 'normal',
        fontWeight: 'bold',
        fontSize: 16,
        lineHeight: 19,
        padding: 12,

        color: '#8E8E8E',
        alignItems: 'center',
        borderWidth: 1,
        borderColor: '#C4C0C0',
    },

    button_access: {
      position:'absolute',
      top:487,
      fontSize: 18,
      width: '80%',
      height: '6.7%',
      backgroundColor: "#F7A04A",
      fontWeight: "bold",
      alignSelf: "center",
      textTransform: "uppercase",
      borderRadius: 50,
    },

    button_access_text: {
      fontFamily:'Lato_100Thin',
      fontWeight: 'bold',
      fontSize: 18,
      color: '#FFF',
      top: '30%',
      left:'40%',
    },

});

export default styles;